"""spine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', 'skin.views.index', name='index'),
    url(r'^lists/$', 'skin.views.lists', name='lists'),
    url(r'^list/', include([
        url(r'^create/$', 'skin.views.create_list', name='create'),
        url(r'^(?P<list_id>\d+)/', include([
            url(r'^$', 'skin.views.list', name='index'),
            url(r'^play/$', 'skin.views.list_play', name='play'),
            url(r'^beadj/$', 'skin.views.list_be_a_dj', name='be_a_dj'),
            url(r'^addamusic/$', 'skin.views.list_add_a_music', name='add_a_music'),
            url(r'^rmamusic/(?P<music_in_list_id>\d+)/$', 'skin.views.list_rm_a_music', name='rm_a_music'),
            url(r'^addayoutubemusic/(?P<yt_id>[-\w]+)/$', 'skin.views.list_add_a_yt_music', name='add_a_yt_music'),
        ], namespace='details')),
        url(r'^playpause/$', 'skin.views.list_music_playpause', name='music_playpause'),
        url(r'^skipmyturn/$', 'skin.views.list_skipmyturn', name='skipmyturn'),
        url(r'^forceplay/$', 'skin.views.list_force_play', name='force_play'),
    ], namespace='list')),
    url(r'^playerapi/(?P<list_id>\d+)/', include([
        url(r'^$', 'skin.views.player_api', name='index'),
        url(r'^started/$', 'skin.views.player_started', name='started'),
        url(r'^play/$', 'skin.views.player_play', name='play'),
        url(r'^pause/$', 'skin.views.player_pause', name='pause'),
        url(r'^ended/$', 'skin.views.player_ended', name='ended'),
        url(r'^updateseconds/$', 'skin.views.player_update_seconds', name='update_seconds'),
    ], namespace='player_api')),
    url(r'^changedjinfo/$', 'skin.views.change_dj', name='change_dj'),

    url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)