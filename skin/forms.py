from brain.models import List, DJ
from django import forms


class ListForm(forms.ModelForm):
    class Meta:
        model = List
        fields = ['title', 'public']

class SearchYT(forms.Form):
    query = forms.CharField(label='')

class ChangeDJ(forms.ModelForm):
    class Meta:
        model = DJ
        fields = ['name']