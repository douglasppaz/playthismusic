import json
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


class Websocket(object):
    @staticmethod
    def send_to_lists(lists, data):
        facilitys = map(lambda x: 'list-{}'.format(x), lists)
        message = RedisMessage(json.dumps(data))
        for facility in facilitys:
            redis_publisher = RedisPublisher(facility=facility, broadcast=True)
            redis_publisher.publish_message(message)
        return

    @staticmethod
    def send_to_listplayer(list, data):
        message = RedisMessage(json.dumps(data))
        facility = 'listplayer-{}'.format(list.id)
        redis_publisher = RedisPublisher(facility=facility, broadcast=True)
        redis_publisher.publish_message(message)
        return