import json
from django.utils.translation import ugettext_lazy as _
from brain.sources.youtube import YoutubeMusic
from django.contrib import messages
from django.http import JsonResponse, HttpResponseNotFound, HttpResponseForbidden
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
import requests
from brain.models import List, DJList, MusicInList
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from skin.forms import ListForm, SearchYT, ChangeDJ


def index(request):
    return redirect(reverse('lists'))

def lists(request):
    return render(request, 'lists/all.html')

@login_required
def create_list(request):
    form = ListForm(request.POST or None)
    if form.is_valid():
        list = form.save(commit=False)
        list.user = request.user
        list.save()
        return redirect(reverse('lists'))
    return render(request, 'lists/create.html', {
        'form': form
    })

def list(request, list_id):
    list = get_object_or_404(List, id=list_id)
    return render(request, 'lists/index.html', {
        'list': list
    })

@login_required
def list_play(request, list_id):
    list = get_object_or_404(List, id=list_id, user=request.user)
    return render(request, 'lists/play.html', {
        'list': list
    })

def list_be_a_dj(request, list_id):
    list = get_object_or_404(List, id=list_id)
    if not list in request.dj.lists.all():
        DJList.objects.create(
            dj=request.dj,
            list=list
        )
    return redirect(reverse('list:details:index', kwargs={'list_id': list.id}))

def list_add_a_music(request, list_id):
    list = get_object_or_404(List, id=list_id)

    form = SearchYT(request.POST or None)

    yt_results = []

    if form.is_valid():
        from urllib import quote
        q = quote(form.cleaned_data.get('query').encode('utf8'))
        r = requests.get('https://www.googleapis.com/youtube/v3/search?part=snippet&q={}&type=video&maxResults=20&key={}'.format(
            q,
            settings.YOUTUBE_API_KEY
        ))
        json_loaded = json.loads(r.text)
        yt_results = json_loaded.get('items')

    return render(request, 'lists/add_a_music.html', {
        'list': list,
        'form': form,
        'yt_results': yt_results
    })

def list_add_a_yt_music(request, list_id, yt_id):
    list = get_object_or_404(List, id=list_id)
    if list in request.dj.lists.all():
        try:
            ytmusic = YoutubeMusic.objects.get(yt_id=yt_id)
        except:
            youtube_url = 'https://www.youtube.com/watch?v={}'.format(yt_id)
            ytmusic, created = YoutubeMusic.objects.get_or_create(
                url=youtube_url
            )
        try:
            djlist = DJList.objects.get(
                dj=request.dj,
                list=list
            )
            MusicInList.objects.create(
                djlist=djlist,
                music=ytmusic,
            )
        except Exception, e:
            print e.message
    return redirect(reverse('list:details:index', kwargs={'list_id': list.id}))

def list_rm_a_music(request, list_id, music_in_list_id):
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__dj=request.dj)
    musicinlist.delete()
    return redirect(reverse('list:details:index', kwargs={'list_id': musicinlist.djlist.list.id}))

@csrf_exempt
def list_music_playpause(request):
    list_id = request.POST.get('list_id')
    list = get_object_or_404(List, id=list_id)
    if not list.now:
        return HttpResponseNotFound()
    if not list.now.djlist.dj == request.dj and not list.user == request.user:
        return HttpResponseForbidden()
    list.playpause()
    return JsonResponse({})

@csrf_exempt
def list_skipmyturn(request):
    list_id = request.POST.get('list_id')
    list = get_object_or_404(List, id=list_id)
    if not list.now:
        return HttpResponseNotFound()
    if not list.now.djlist.dj == request.dj and not list.user == request.user:
        return HttpResponseForbidden()
    list.playnext()
    return JsonResponse({})

@csrf_exempt
def list_force_play(request):
    music_in_list_id = request.POST.get('music_in_list_id')
    music_in_list = get_object_or_404(MusicInList, id=music_in_list_id)
    if not music_in_list.djlist.list.user == request.user:
        return HttpResponseForbidden()
    music_in_list.top()
    music_in_list.djlist.list.playnext()
    return JsonResponse({})

@login_required
def player_api(request, list_id):
    list = get_object_or_404(List, id=list_id, user=request.user)
    return JsonResponse({
        'now': list.now.serializer() if list.now else False
    })

@login_required
@csrf_exempt
def player_started(request, list_id):
    music_in_list_id = request.POST.get('music_in_list_id')
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__list__user=request.user)
    musicinlist.started = timezone.now()
    musicinlist.save(update_fields=['started'])
    return JsonResponse({})

@login_required
@csrf_exempt
def player_play(request, list_id):
    music_in_list_id = request.POST.get('music_in_list_id')
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__list__user=request.user)
    musicinlist.playing = True
    musicinlist.save(update_fields=['playing'])
    return JsonResponse({})

@login_required
@csrf_exempt
def player_pause(request, list_id):
    music_in_list_id = request.POST.get('music_in_list_id')
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__list__user=request.user)
    musicinlist.playing = False
    musicinlist.save(update_fields=['playing'])
    return JsonResponse({})

@login_required
@csrf_exempt
def player_ended(request, list_id):
    music_in_list_id = request.POST.get('music_in_list_id')
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__list__user=request.user)
    musicinlist.delete()
    return JsonResponse({})

@login_required
@csrf_exempt
def player_update_seconds(request, list_id):
    music_in_list_id = request.POST.get('music_in_list_id')
    musicinlist = get_object_or_404(MusicInList, id=music_in_list_id, djlist__list__id=list_id, djlist__list__user=request.user)
    musicinlist.seconds = int(request.POST.get('seconds'))
    musicinlist.save(update_fields=['seconds'])
    return JsonResponse({})

def change_dj(request):
    form = ChangeDJ(request.POST or None, instance=request.dj)
    if form.is_valid():
        messages.success(request, _(u'Your dj name was changed successfully!!'))
        form.save()
    return render(request, 'change_dj.html', {
        'form': form
    })