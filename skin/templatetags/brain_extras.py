from brain.models import DJList
from django import template

register = template.Library()

@register.assignment_tag
def get_dj_list(dj, list):
    try:
        return DJList.objects.get(dj=dj, list=list)
    except:
        return False

@register.simple_tag(takes_context=True)
def upcoming_render_with_request(context, music_in_list):
    print music_in_list
    return music_in_list.upcoming_render(request=context.get('request'))