var reload = false;

function connect_websocket(list_id){
    var ws_url = 'ws://'+window.location.hostname+':'+window.location.port+'/ws/list-'+list_id+'?subscribe-broadcast',
        ws = new WebSocket(ws_url);
    ws.onmessage = function(e) {
        var data = JSON.parse(e.data);
        switch(data.act){
            case 'YTdebug':
                var $YTdebug = $('[data-YTdebug='+data.instance_id+']');
                $YTdebug
                    .show()
                    .html(data.debug);
                if($YTdebug.html() == ''){
                    $YTdebug.hide();
                }
                break;
            case 'YTseconds':
                var $YTseconds = $('[data-YTseconds='+data.instance_id+']');
                $YTseconds.html(data.seconds_str);
                break;
            case 'YTplaying':
                var $YTplaying = $('[data-YTplaying='+data.instance_id+']'),
                    $playingIcon = $YTplaying.find('.playing-icon');
                if(data.playing){
                    $playingIcon
                        .removeClass('glyphicon-play')
                        .addClass('glyphicon-pause');
                } else {
                    $playingIcon
                        .removeClass('glyphicon-pause')
                        .addClass('glyphicon-play');
                }
                break;
            case 'player_ended':
                if(reload){
                    location.reload();
                }
                break;
            case 'player_started':
                if(reload){
                    location.reload();
                }
                break;
            default :
                console.log(data);
                break;
        }
    };
}

function playpausemusic(list_id){
    $.ajax({
        type: 'POST',
        url: '/list/playpause/',
        data: {
            list_id: list_id
        },
        success: function (data){}
    });
}
function skipmyturn(list_id){
    $.ajax({
        type: 'POST',
        url: '/list/skipmyturn/',
        data: {
            list_id: list_id
        },
        success: function (data){}
    });
}

function play(music_in_list_id){
    $.ajax({
        type: 'POST',
        url: '/list/forceplay/',
        data: {
            music_in_list_id: music_in_list_id
        },
        success: function (data){}
    });
}

$(document).ready(function (){
    $('[data-YTdebug]').each(function (i, YTdebug){
        var $YTdebug = $(YTdebug);
        if($YTdebug.html() == ''){
            $YTdebug.hide();
        }
    });
});