import logging
from brain.models import MusicInList
from brain.sources.youtube import YoutubeMusic
from django.db.models import Q

logger = logging.getLogger('source__youtube')

def do():
    result = YoutubeMusic.objects.filter(
        Q(yt_id__isnull=True) | Q(title__isnull=True) | Q(yt_id='') | Q(title='') | Q(duration=-1)
    )
    for ytm in result:
        ytm.update_infos()

    result = MusicInList.objects.filter(
        music___my_subclass='youtubemusic',
    )

    if result:
        for music_in_list in result:
            if not music_in_list.music.as_child().file:
                music_in_list.music.as_child().download()
                break
    else:
        result = YoutubeMusic.objects.filter(Q(file__isnull=True) | Q(file=''))
        for ytm in result:
            ytm.download()
            break

    result = YoutubeMusic.objects.filter(
        normalized=False,
        file__isnull=False
    )

    for ytm in result:
        ytm.normalize()
        break