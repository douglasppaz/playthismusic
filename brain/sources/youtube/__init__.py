# coding=utf-8
import logging
from django.db import models
from brain.models import Music, MusicInList
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import loader
from django.utils.translation import ugettext_lazy as _
from skin import Websocket

logger = logging.getLogger('source__youtube')

class YoutubeLogger(object):
    _ytm = None

    def __init__(self, ytm=None):
        self._ytm = ytm

    def debug(self, msg):
        if self._ytm:
            self._ytm.debug = msg
            self._ytm.save(update_fields=['debug'])
            logger.debug(msg)

    def warning(self, msg):
        logger.warning(msg)

    def error(self, msg):
        logger.error(msg)

class YoutubeMusic(Music):
    url = models.URLField(_(u'URL'), unique=True)
    yt_id = models.CharField(_(u'Youtube Video ID'), max_length=16, null=True, blank=True)
    title = models.CharField(_(u'title'), max_length=256, null=True, blank=True)
    duration = models.IntegerField(_(u'duration in seconds'), default=-1)
    file = models.FileField(_(u'video file'), upload_to='youtubevideo', null=True, blank=True)
    thumbnail = models.ImageField(_(u'thumbnail'), upload_to='youtubethumbnail', null=True, blank=True)
    debug = models.CharField(_(u'debug'), max_length=256, null=True, blank=True)
    normalized = models.BooleanField(_(u'audio normalized?'), default=False)

    def __unicode__(self):
        return u'Youtube Music [{}]'.format(
            u'{}] [{}'.format(self.title, self.duration_str) if self.title else self.url
        )

    @property
    def type(self):
        return 'youtubemusic' if self.file else 'youtubeclassic'

    def extra_serializer(self):
        return {
            'yt_id': self.yt_id
        }

    def update_infos(self, save=True):
        import requests, json
        from django.conf import settings
        from urlparse import urlparse

        urlparsed = urlparse(self.url)
        if urlparsed.netloc in ['www.youtube.com', 'youtube.com']:
            if urlparsed.path == '/watch' and urlparsed.query.startswith('v='):
                self.yt_id = urlparsed.query.replace('v=', '')
        elif urlparsed.netloc in ['youtu.be', 'www.youtu.be']:
            self.yt_id = urlparsed.path.replace('/', '')

        if self.yt_id:
            request = requests.get('https://www.googleapis.com/youtube/v3/videos?id={}&key={}&part=snippet,contentDetails,statistics,status'.format(
                self.yt_id,
                settings.YOUTUBE_API_KEY
            ))
            dict = json.loads(request.text)
            items = dict.get('items')
            if items:
                import isodate, shutil
                from tempfile import mkstemp
                from django.core.files import File

                item = items[0]
                self.title = u'{}'.format(item.get('snippet').get('title').encode('utf-8').decode('utf-8'))
                self.duration = isodate.parse_duration(item.get('contentDetails').get('duration')).total_seconds()

                thumb_url = item.get('snippet').get('thumbnails').get('high').get('url')
                r = requests.get(thumb_url, stream=True)
                i, thumb_temp = mkstemp()
                if r.status_code == 200:
                    with open(thumb_temp, 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                    f = open(thumb_temp)
                    self.thumbnail.save(thumb_url.split('/')[-1], File(f))

            if save:
                self.save()
            return True
        return False

    def download(self):
        try:
            import youtube_dl
            from tempfile import mkstemp

            def my_hook(d):
                if d.get('status') == 'finished':
                    from django.core.files import File
                    f = open(d.get('filename'))
                    self.file.save(d.get('filename').split('/')[-1], File(f))
                    self.debug = None
                    self.normalized = False
                    self.save(update_fields=['file', 'debug', 'normalized'])
                    f.close()

            i, outtmpl = mkstemp('%(title)s-%(id)s.%(ext)s')

            ydl_opts = {
                'format': 'best',
                'logger': YoutubeLogger(self),
                'progress_hooks': [my_hook],
                'restrictfilenames': True,
                'outtmpl': outtmpl
            }
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([self.url])
            return True
        except Exception, e:
            logger.error(e.message)
            return False

    def normalize(self):
        if self.file:
            import subprocess, os
            from tempfile import mkstemp
            from django.core.files import File

            logger.info(u'Normalize {}'.format(self.title))

            old_file = self.file.path
            filename = self.file.path.split('/')[-1]
            i, temp_file = mkstemp(filename)

            command = [
                'ffmpeg',
                '-y',
                '-i',
                old_file,
                '-af',
                '"volume=0dB"',
                '-c:v',
                'copy',
                '-c:a',
                'aac',
                '-strict',
                'experimental',
                '-b:a',
                '320k',
                temp_file
            ]

            subprocess.call(' '.join(command), shell=True)

            f = open(self.file.path)
            self.file.save(filename, File(f))
            self.normalized = True
            self.save(update_fields=['file', 'normalized'])
            f.close()
            os.remove(old_file)

        return True

    def upcoming_render(self, music_in_list=None, request=None):
        return loader.render_to_string('upcoming_yt.html', context={
            'music': self,
            'music_in_list': music_in_list
        }, request=request)

    def player_render(self):
        if self.file:
            return loader.render_to_string('player_yt.html', context={
                'music': self,
            })
        return loader.render_to_string('player_yt_classic.html', context={'music': self})

    def send_debug_to_websocket(self):
        Websocket.send_to_lists(self.lists_id, {
            'act': 'YTdebug',
            'instance_id': self.id,
            'debug': self.debug or ''
        })
        return True

    @property
    def file_mime(self):
        if not self.file:
            return False
        import magic
        mime = magic.Magic(mime=True)
        return mime.from_file(self.file.path)

@receiver(post_save, sender=YoutubeMusic)
def post_save(instance, update_fields, created, **kwargs):
    if update_fields and 'debug' in update_fields:
        instance.send_debug_to_websocket()

    if created:
        instance.update_infos()