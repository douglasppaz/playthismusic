from brain.models import Music, DJ, DJList, List, MusicInList
from brain.sources.youtube import YoutubeMusic
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from ordered_model.admin import OrderedModelAdmin

admin.site.register(Music)
admin.site.register(DJ)
admin.site.register(List)

@admin.register(YoutubeMusic)
class YoutubeMusicAdmin(admin.ModelAdmin):
    def _youtube_id(self, instance):
        return u'{}'.format(instance.yt_id if instance.yt_id else _('#{0} no update').format(instance.id))

    def _title(self, instance):
        return u'{}'.format(instance.title if instance.title else _('no update'))

    def _duration(self, instance):
        return instance.duration_str

    def _debug(self, instance):
        return u'{}'.format(instance.debug if instance.debug else _('no update'))

    list_display = ['_youtube_id', '_title', '_duration', '_debug']
    search_fields = ['title']
    readonly_fields = ['title', '_duration', 'file', 'debug']
    fields = ['url', 'normalized'] + readonly_fields

@admin.register(DJList)
class DJListAdmin(OrderedModelAdmin):
    list_display = ['__str__', 'move_up_down_links']

@admin.register(MusicInList)
class MusicInListAdmin(OrderedModelAdmin):
    list_display = ['__str__', 'move_up_down_links']