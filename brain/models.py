from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from ordered_model.models import OrderedModel
from skin import Websocket


class KnowsChild(models.Model):
    _my_subclass = models.CharField(max_length=256, blank=True)

    class Meta:
        abstract = True

    def as_child(self):
        try:
            return getattr(self, self._my_subclass)
        except Exception, e:
            return False

    def save(self, *args, **kwargs):
        self._my_subclass = self.__class__.__name__.lower()
        super(KnowsChild, self).save(*args, **kwargs)

    @staticmethod
    def always_as_child(fn):
        def f(self, *args, **kwargs):
            child_self = self.as_child()
            f_parent = getattr(self.__class__, fn.__name__)
            f_child = getattr(child_self.__class__, fn.__name__)

            if f_parent != f_child:
                return f_child(child_self, *args, **kwargs)
            else:
                return fn(self, *args, **kwargs)
        return f

class Music(KnowsChild):
    def __unicode__(self):
        return self.as_child().__unicode__() if self.as_child() else _(u'Music #{}').format(self.id)

    @property
    def lists_id(self):
        return list(set(MusicInList.objects.filter(music=self).values_list('djlist__list', flat=True)))

    _duration_str = False
    @property
    def duration_str(self):
        if not self._duration_str:
            self._duration_str = u'{}:{}'.format('%02d' % (self.as_child().duration/60), '%02d' % (self.as_child().duration%60)) if hasattr(self.as_child(), 'duration') else False if self.as_child() else False
        return self._duration_str

    def upcoming_render(self, music_in_list=None, request=None):
        return self.as_child().upcoming_render(music_in_list=music_in_list, request=request) if self.as_child() else u''

    def player_render(self):
        return self.as_child().player_render() if self.as_child() else u''

    @property
    def type(self):
        return self.as_child().type if self.as_child() else 'music'

    def serializer(self):
        return {
            'id': self.id,
            'player_render': self.player_render(),
            'type': self.type,
            'extra': self.extra_serializer()
        }

    def extra_serializer(self):
        return self.as_child().extra_serializer() if hasattr(self.as_child(), 'extra_serializer') else {} if self.as_child() else {}

class List(models.Model):
    code = models.CharField(_(u'code'), max_length=4, blank=True)
    title = models.CharField(_(u'title'), max_length=128)
    user = models.ForeignKey(User)
    public = models.BooleanField(_(u'public?'), default=True)

    def __unicode__(self):
        return u'[{}] {}'.format(self.code, self.title)

    @property
    def upcoming(self):
        return MusicInList.objects.filter(djlist__list=self, started__isnull=True)

    _now = None
    @property
    def now(self):
        if not self._now:
            self._now = MusicInList.objects.filter(djlist__list=self, started__isnull=False).first() or MusicInList.objects.filter(djlist__list=self).first() or False
        return self._now

    def playpause(self):
        if self.now:
            Websocket.send_to_listplayer(self, {
                'act': 'playpause',
                'playing': self.now.playing
            })
        return True

    def playnext(self):
        if self.now:
            Websocket.send_to_listplayer(self, {
                'act': 'playnext'
            })
        return True

    def send_music_ended_to_websocket(self):
        Websocket.send_to_lists([self.id], {
            'act': 'player_ended',
        })
        return True

    def send_music_started_to_websocket(self):
        Websocket.send_to_lists([self.id], {
            'act': 'player_ended',
        })
        return True

@receiver(pre_save, sender=List)
def presave_List(instance, **kwargs):
    import random, string

    if not instance.code:
        while True:
            instance.code = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
            if not List.objects.filter(code=instance.code):
                break

class DJ(models.Model):
    AVATAR_CHOICES = [
        (1, '')
    ]

    token = models.CharField(_(u'token'), max_length=64, unique=True)
    name = models.CharField(_(u'name'), max_length=16, null=True, blank=True)
    avatar = models.IntegerField(_(u'avatar'), choices=AVATAR_CHOICES, default=0)
    lists = models.ManyToManyField(List, through='DJList')

    def __unicode__(self):
        return u'DJ {}'.format(self.name if self.name else self.token)

    @property
    def lists_available(self):
        ids = map(lambda x: x.get('id'), self.lists.all().values('id'))
        return List.objects.filter(Q(id__in=ids) | Q(public=True)).distinct()

@receiver(pre_save, sender=DJ)
def presave_DJ(instance, **kwargs):
    import random, string

    if not instance.token:
        while True:
            instance.token = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(64))
            if not DJ.objects.filter(token=instance.token):
                break

    if not instance.name:
        instance.name = random.choice([
            'Coala',
            'Girafa',
            'Azul',
            'Arara',
            'Pinguim',
            '',
        ])

class DJList(OrderedModel):
    class Meta:
        ordering = ['list', 'order']
        unique_together = ('dj', 'list')
    order_with_respect_to = 'list'

    dj = models.ForeignKey(DJ)
    list = models.ForeignKey(List, related_name='djlists')

    def __unicode__(self):
        return _(u'{} in list {}').format(self.dj.__unicode__(), self.list.__unicode__())

    @property
    def str_order(self):
        return self.order + 1

class MusicInList(OrderedModel):
    class Meta:
        ordering = ['djlist__list', 'order']
    order_with_respect_to = 'list_id'

    djlist = models.ForeignKey(DJList, related_name='musics')
    list_id = models.IntegerField(default=0)
    music = models.ForeignKey(Music)
    added = models.DateTimeField(_(u'added'), auto_now_add=True)
    started = models.DateTimeField(_(u'started?'), null=True)
    seconds = models.IntegerField(_(u'seconds'), default=0)
    playing = models.BooleanField(_(u'playing?'), default=False)

    def __unicode__(self):
        return u'{} - {} - {}'.format(self.djlist.list.__unicode__(), self.djlist.dj.__unicode__(), self.music.__unicode__())

    @staticmethod
    def order_list(l):
        search = MusicInList.objects.filter(djlist__list=l, started__isnull=True)
        djs_id = sorted(list(set(search.values_list('djlist__dj', flat=True))))
        djs_musics = {}
        for dj_id in djs_id:
            djs_musics.update({
                dj_id: list(search.filter(djlist__dj__id=dj_id).order_by('added'))
            })
        c = 0
        while False in map(lambda x: len(djs_musics[x])==0, djs_musics.keys()):
            for dj_id in djs_id:
                try:
                    djs_musics[dj_id][0].to(c)
                    djs_musics[dj_id].pop(0)
                    c += 1
                except Exception, e:
                    print e

    @property
    def str_order(self):
        return self.order + 1

    @property
    def seconds_str(self):
        return u'{}:{}'.format('%02d' % (self.seconds/60), '%02d' % (self.seconds%60))

    def upcoming_render(self, request=None):
        return self.music.upcoming_render(music_in_list=self, request=request)

    def serializer(self):
        return {
            'id': self.id,
            'music': self.music.serializer(),
            'seconds': self.seconds,
        }

    def send_seconds_to_websocket(self):
        Websocket.send_to_lists([self.list_id], {
            'act': 'YTseconds',
            'instance_id': self.id,
            'seconds': self.seconds,
            'seconds_str': self.seconds_str,
        })
        return True

    def send_playing_to_websocket(self):
        Websocket.send_to_lists([self.list_id], {
            'act': 'YTplaying',
            'instance_id': self.id,
            'playing': self.playing,
        })
        return True

@receiver(post_save, sender=MusicInList)
def post_save_MusicInList(instance, update_fields, created, **kwargs):
    if update_fields and 'started' in update_fields:
        instance.djlist.list.send_music_started_to_websocket()

    if update_fields and 'seconds' in update_fields:
        instance.send_seconds_to_websocket()

    if update_fields and 'playing' in update_fields:
        instance.send_playing_to_websocket()

    if created:
        instance.bottom()
        instance.list_id = instance.djlist.list.id
        instance.save()
        instance.order_list(instance.djlist.list)

@receiver(post_delete, sender=MusicInList)
def post_delete_MusicInList(instance, **kwargs):
    instance.djlist.list.send_music_ended_to_websocket()

from brain.sources.youtube import YoutubeMusic