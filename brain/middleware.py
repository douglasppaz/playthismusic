from brain.models import DJ


class DJMiddleware(object):
    def new_dj(self, request):
        dj = DJ.objects.create()
        request.session.update({
            '_dj_token': dj.token
        })
        return dj

    def process_request(self, request):
        if not request.session.get('_dj_token', False):
            dj = self.new_dj(request)
        else:
            try:
                dj = DJ.objects.get(token=request.session.get('_dj_token'))
            except:
                dj = self.new_dj(request)
        request.dj = dj