from time import sleep
from django.core.management import BaseCommand

from brain.sources.youtube import command as YTcommand

class Command(BaseCommand):
    help = 'Start Sources'

    def handle(self, *args, **options):
        while True:
            YTcommand.do()
            sleep(1)