# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brain', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='musicinlist',
            options={'ordering': ['djlist__list', 'order']},
        ),
        migrations.AddField(
            model_name='musicinlist',
            name='list_id',
            field=models.IntegerField(default=0),
        ),
    ]
