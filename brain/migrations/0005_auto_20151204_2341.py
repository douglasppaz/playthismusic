# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brain', '0004_auto_20151204_2340'),
    ]

    operations = [
        migrations.RenameField(
            model_name='youtubemusic',
            old_name='normalize',
            new_name='normalized',
        ),
    ]
