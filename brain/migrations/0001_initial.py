# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DJ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(unique=True, max_length=64, verbose_name='token')),
                ('name', models.CharField(max_length=16, null=True, verbose_name='name', blank=True)),
                ('avatar', models.IntegerField(default=0, verbose_name='avatar', choices=[(1, b'')])),
            ],
        ),
        migrations.CreateModel(
            name='DJList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(editable=False, db_index=True)),
                ('dj', models.ForeignKey(to='brain.DJ')),
            ],
            options={
                'ordering': ['list', 'order'],
            },
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=4, verbose_name='code', blank=True)),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('public', models.BooleanField(default=True, verbose_name='public?')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Music',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_my_subclass', models.CharField(max_length=256, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MusicInList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(editable=False, db_index=True)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='added')),
                ('started', models.DateTimeField(null=True, verbose_name='started?')),
                ('seconds', models.IntegerField(default=0, verbose_name='started played')),
                ('djlist', models.ForeignKey(related_name='musics', to='brain.DJList')),
            ],
            options={
                'ordering': ['order', 'djlist__list'],
            },
        ),
        migrations.CreateModel(
            name='YoutubeMusic',
            fields=[
                ('music_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='brain.Music')),
                ('url', models.URLField(unique=True, verbose_name='URL')),
                ('yt_id', models.CharField(max_length=16, null=True, verbose_name='Youtube Video ID', blank=True)),
                ('title', models.CharField(max_length=256, null=True, verbose_name='title', blank=True)),
                ('duration', models.IntegerField(default=-1, verbose_name='duration in seconds')),
                ('file', models.FileField(upload_to=b'youtubevideo', null=True, verbose_name='video file', blank=True)),
                ('thumbnail', models.ImageField(upload_to=b'youtubethumbnail', null=True, verbose_name='thumbnail', blank=True)),
                ('debug', models.CharField(max_length=256, null=True, verbose_name='debug', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('brain.music',),
        ),
        migrations.AddField(
            model_name='musicinlist',
            name='music',
            field=models.ForeignKey(to='brain.Music'),
        ),
        migrations.AddField(
            model_name='djlist',
            name='list',
            field=models.ForeignKey(related_name='djlists', to='brain.List'),
        ),
        migrations.AddField(
            model_name='dj',
            name='lists',
            field=models.ManyToManyField(to='brain.List', through='brain.DJList'),
        ),
        migrations.AlterUniqueTogether(
            name='djlist',
            unique_together=set([('dj', 'list')]),
        ),
    ]
