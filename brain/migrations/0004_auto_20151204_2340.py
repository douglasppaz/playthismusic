# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brain', '0003_musicinlist_playing'),
    ]

    operations = [
        migrations.AddField(
            model_name='youtubemusic',
            name='normalize',
            field=models.BooleanField(default=False, verbose_name='audio normalized?'),
        ),
        migrations.AlterField(
            model_name='musicinlist',
            name='seconds',
            field=models.IntegerField(default=0, verbose_name='seconds'),
        ),
    ]
