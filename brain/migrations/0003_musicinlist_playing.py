# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brain', '0002_auto_20151202_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='musicinlist',
            name='playing',
            field=models.BooleanField(default=False, verbose_name='playing?'),
        ),
    ]
